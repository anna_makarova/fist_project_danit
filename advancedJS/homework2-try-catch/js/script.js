"use strict";
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const root = document.getElementById("root");
function displayAList(array, parent) {
  const ul = document.createElement("ul");

  array.forEach((book) => {
    const { author, name, price } = book;

    try {
      if (author && name && price) {
        ul.innerHTML += `<li><p>${author}</p><p>${name}</p><p>${price}</p></li>`;
      } else if (!author) {
        throw new SyntaxError(`Cвойсвa author нет в обьекте`);
      } else if (!price) {
        throw new SyntaxError(`Cвойсвa price нет в обьекте`);
      } else if (!name) {
        throw new SyntaxError(`Cвойсвa name нет в обьекте`);
      }
    } catch (e) {
      console.log(e.name, e.message);
    }

    parent.append(ul);
  });
}

displayAList(books, root);
