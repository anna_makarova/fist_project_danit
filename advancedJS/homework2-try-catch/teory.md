Приведите пару примеров, когда уместно использовать в коде конструкцию `try...catch`.
Конструкцию `try...catch` правильно использовать в тех случаях если есть или могут быть промахи в нашем коде, в случае когда мы получаем информацию от пользователей, или непрвильнрого ответа из сервера.
Если есть ошибка в коде, скрипт сразу же останавливается (падает), если код записан в конструкцию `try..catch`, код на моменте ошибки переходит в `catch` и продолжает чтения с выводом ошибки в консоль.
