import reducer from "./reducer";
import actions from "./actions";
const initialState = {
  isOpen: false,
  cardToDelete: null,
  data: [{ id: 1 }],
};
const mockCard = { id: 1 };

test("CART_IS_OPEN action changes isOpen prop in state", () => {
  const action = actions.cartIsOpen(true);
  const newState = reducer(initialState, action);
  expect(newState.isOpen).toBe(true);
});

test("SET_CARD_TO_DELETE action determinate cardToDelete prop in state", () => {
  const action = actions.setCardToDelete(mockCard);
  const newState = reducer(initialState, action);
  expect(newState.cardToDelete).toBe(mockCard);
});

test("SET_CARTS_DATA action determinate cardToDelete prop in state", () => {
  const mockArr = initialState.data.filter((el) => el.id !== mockCard.id);
  const action = actions.setCartData(mockArr);
  const newState = reducer(initialState, action);
  expect(newState.data).toBe(mockArr);
});

test("CLEAR_CART action clearCart prop in state", () => {
  const action = actions.setCartData(null);
  const newState = reducer(initialState, action);
  expect(newState.data).toBeNull();
});
