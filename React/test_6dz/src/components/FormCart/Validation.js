import * as Yup from "yup";

const schema = Yup.object().shape({
  name: Yup.string()
    .required("This field is required")
    .min(2, "Please enter full name")
    .max(30, "Too Long!"),
  secondName: Yup.string()
    .min(3, "Please enter full second name")
    .required("This field is required")
    .max(30, "Too Long!"),
  age: Yup.number()
    .required()
    .integer()
    .min(16, "Sorry, but you`re too young")
    .max(100, "Please enter your true age"),
  adress: Yup.string()
    .min(10, "Please confirm full address")
    .required("This field is required"),
  phone: Yup.number()
    .typeError("That doesn't look like a phone number")
    .min(9, "This is not a valid phone")
    .required("This field is required")
    .positive("A phone number can't start with a minus"),
});

export default schema;
// const PHONE_REGEX = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
// const validate = (values) => {
//   const { name, secondName, age, adress, phone } = values;

//   const errors = {};
//   if (!name.trim()) {
//     errors.name = "This fild is required";
//   } else if (name.length < 2) {
//     errors.name = "Please enter full name";
//   }
//   if (!secondName.trim()) {
//     errors.secondName = "This fild is required";
//   } else if (secondName.length < 3) {
//     errors.secondName = "Please enter full second name ";
//   }
//   if (!age) {
//     errors.age = "This fild is required";
//   } else if (age < 16) {
//     errors.age = "Sorry, but you're too young";
//   } else if (age > 100) {
//     errors.age = "Please enter your true age";
//   }
//   if (!adress.trim()) {
//     errors.adress = "This fild is required";
//   } else if (adress.length < 10) {
//     errors.adress = "Please confirm full address";
//   }
//   if (!phone.trim()) {
//     errors.phone = "This fild is required";
//   } else if (!PHONE_REGEX.test(phone)) {
//     errors.phone = "This is not a valid phone";
//   }
//   return errors;
// };
