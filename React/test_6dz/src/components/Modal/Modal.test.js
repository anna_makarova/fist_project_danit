import React from "react";
import { Modal } from "./Modal";
import { render } from "@testing-library/react";

test("Smoke test of Modal", () => {
  render(<Modal />);
});

test("Modal test with props toggleModal", () => {
  const toggleModal = jest.fn();
  render(<Modal toggleModal={toggleModal} />);
  expect(toggleModal).toBeDefined();
  expect(toggleModal).not.toHaveBeenCalled();
});
test("Modal test with props isOpen", () => {
  const isOpen = false;
  render(<Modal isOpen={isOpen} />);
  expect(isOpen).toBeFalsy();
});
test("Modal test with props addToCart", () => {
  const addToCart = jest.fn();
  render(<Modal addToCart={addToCart} />);
  expect(addToCart).toBeDefined();
});
test("Modal test with props headerText", () => {
  const headerText = "Some text for header";
  const { getByText } = render(<Modal headerText={headerText} />);
  getByText(/some text for header/i);
});
test("Modal test with props modalText", () => {
  const modalText = "Some text for modal";
  const { getByText } = render(<Modal modalText={modalText} />);
  getByText(/some text for modal/i);
});
test("Modal test with props bntText", () => {
  const buttonText = "Some text for button";
  const { getByText } = render(<Modal btnText={buttonText} />);
  getByText(/some text for button/i);
});
test("Modal test with props openDeleteModal", () => {
  const mockFn = jest.fn();
  render(<Modal addTopenDeleteModaloCart={mockFn} />);
  expect(mockFn).toBeDefined();
});
test("Modal test with props openDeleteModal", () => {
  const mockFn = jest.fn();
  render(<Modal deleteFromCart={mockFn} />);
  expect(mockFn).toBeDefined();
});
test("Snapshot test for Modal", () => {
  const { container } = render(<Modal />);
  expect(container.innerHTML).toMatchSnapshot();
});
