import React, { Component } from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import "./cardList.scss";

class CardsList extends Component {
  render() {
    const {
      goods,
      openModal,
      closeModal,
      modalOpen,
      changeFavorite,
      addToCart,
    } = this.props;
    const goodsList = goods.map((gds) => (
      <Card
        key={gds.id}
        card={gds}
        openModal={openModal}
        closeModal={closeModal}
        modalOpen={modalOpen}
        changeFavorite={() => changeFavorite(gds.id)}
        addToCart={addToCart}
      />
    ));
    return (
      <div className="main-wrapper">
        <header className="header">
          Ниже представленые книги которые есть в наличии
        </header>
        <div className="container">
          <ol className="cards">{goodsList}</ol>;
        </div>
      </div>
    );
  }
}
CardsList.propTypes = {
  goods: PropTypes.array.isRequired,
  changeFavorite: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  modalOpen: PropTypes.bool,
  addToCart: PropTypes.func.isRequired,
};
CardsList.defaultProps = {
  modalOpen: false,
};
export default CardsList;
