import React from "react";
import "./button.scss";
import PropTypes from "prop-types";

const Button = ({ openModal, card }) => {
  return (
    <div>
      <button className="btn" onClick={() => openModal(card)}>
        Add to cart
      </button>
    </div>
  );
};

Button.propTypes = {
  openModal: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired,
};
export default Button;
