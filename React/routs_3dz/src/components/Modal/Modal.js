import React from "react";
import "./modal.scss";
import PropTypes from "prop-types";

const Modal = ({
  closeModal,
  addToCart,
  headerText,
  modalText,
  btnText,
  openDeleteModal,
  open,
  deleteFromCart,
}) => {
  return (
    <div
      onClick={open ? openDeleteModal : closeModal}
      className="modal-container"
    >
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <header className="modal__header">
          <p className="modal__header-text">{headerText}</p>
          {open ? (
            <button
              onClick={openDeleteModal}
              className="modal__header-btn"
            ></button>
          ) : (
            <button onClick={closeModal} className="modal__header-btn"></button>
          )}
        </header>
        <div className="modal__wrap">
          <p className="modal__wrap-text">{modalText}</p>
          <div className="btn-wrap">
            {open ? (
              <button onClick={deleteFromCart} className="modal__btn">
                {btnText}
              </button>
            ) : (
              <button onClick={addToCart} className="modal__btn">
                {btnText}
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
Modal.propTypes = {
  closeModal: PropTypes.func,
  addToCart: PropTypes.func,
};
export default Modal;
