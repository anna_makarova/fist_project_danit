import React from "react";
import "./modal.scss";

class Modal extends React.Component {
  render() {
    const { headerText, closeButton, text, action, closeModal, dataId } =
      this.props;

    return (
      <div onClick={closeModal} data-id={dataId} className="modal-container">
        <div className="modal">
          <header className="modal__header">
            <p className="modal__header-text">{headerText}</p>
            {closeButton && (
              <button
                onClick={closeModal}
                className="modal__header-btn"
              ></button>
            )}
          </header>
          <div className="modal__wrap">
            <p className="modal__wrap-text">{text}</p>
            {action}
          </div>
        </div>
      </div>
    );
  }
}
export default Modal;
